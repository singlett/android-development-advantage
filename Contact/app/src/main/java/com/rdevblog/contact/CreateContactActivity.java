package com.rdevblog.contact;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;


public class CreateContactActivity extends Activity implements CreateContactFragment.OnSaveClicked{

    @Override
    public void onSaveClicked() {
        //Do Nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();

        //Todo up button
        getActionBar().setDisplayHomeAsUpEnabled(true);


        //set the framelayout a fragent
        setContentView(R.layout.activity_create_contact);
        if (savedInstanceState == null) {
            Fragment createContactFragment = new CreateContactFragment();
            createContactFragment.setArguments(extras);
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container,  createContactFragment)
                    .commit();
        }




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


}
