package com.rdevblog.contact;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

//TODO Maybe add pictures

//Todo Material theme

public class MainActivity extends Activity implements CreateContactFragment.OnSaveClicked{

    private ContactListAdapter mContactArrayAdapter;

    private ArrayList<Contact> contactArrayList;

    private Cursor mCursor;

    boolean mIsDualPane;

    FrameLayout contactView;

    @Override
    public void onSaveClicked() {
        //interface handle for dual pane mode that refreshes the list view when an item is clicked in the other fragment.
        this.refreshListview();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the database singleton
        SQLiteDatabase database = ContactsDatabaseHelper.getInstance(getApplicationContext()).getReadableDatabase();


        String query = "SELECT * FROM contacts";

        mCursor = database.rawQuery(query,null);

        //create an array list
        contactArrayList = new ArrayList<Contact>(ContactsDatabaseHelper.getInstance(getApplicationContext()).getAllContacts());

        //create the custom cursor adapter
        mContactArrayAdapter = new ContactListAdapter(getApplicationContext(), mCursor, 0);

        //get the framelayout for the list fragment if the app is in dual pane mode on tablets.
        contactView = (FrameLayout)findViewById(R.id.fragment_container);
        mIsDualPane = contactView != null;
        Log.d("Is it visable", String.valueOf(mIsDualPane));

        //get the listview and set the custom adapter.
        ListView listView = (ListView)findViewById(R.id.contactsList);
        listView.setAdapter(mContactArrayAdapter);


        //handle the multiple views or new intents depending on mode.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!mIsDualPane) {
                Intent intent = new Intent(getApplicationContext(), CreateContactActivity.class);
                intent.putExtra("contact_id", id);
                startActivity(intent);
            }
            else {
                contactView.removeAllViews();
                Bundle extras = new Bundle();
                extras.putLong("contact_id", id);
                extras.putBoolean("is_dual_pane", true);
                Fragment createContactFragment = new CreateContactFragment();
                createContactFragment.setArguments(extras);
                getFragmentManager().beginTransaction()
                        .add(R.id.fragment_container,  createContactFragment)
                        .commit();
            }


            }
        });

        //handle a long click on the listview for deleting a contact
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                ContactsDatabaseHelper.getInstance(getApplicationContext()).delete(ContactsDatabaseHelper.getInstance(getApplicationContext()).getContact(id));
                refreshListview();

                return true;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){

            //create a new activity for a new contact or fill the framelayout with a new contact fragment depending on mode
            case R.id.new_contact_menu_item:
                if (!mIsDualPane) {
                    Intent newContactIntent = new Intent(this, CreateContactActivity.class);
                    startActivity(newContactIntent);
                }
                else {
                    contactView.removeAllViews();
                    Bundle extras = new Bundle();
                    extras.putBoolean("is_dual_pane", true);
                    Fragment createContactFragment = new CreateContactFragment();
                    createContactFragment.setArguments(extras);
                    getFragmentManager().beginTransaction()
                            .add(R.id.fragment_container,  createContactFragment)
                            .commit();
                }


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //when the acivity is resumed refresh the list view and handle mode information
        refreshListview();
        if (mIsDualPane){
            contactView.removeAllViews();
        }

    }

    public void refreshListview(){

        //custom method to refresh the contacts in teh database when a new one is made.
        SQLiteDatabase database = ContactsDatabaseHelper.getInstance(getApplicationContext()).getReadableDatabase();
        Cursor c = database.rawQuery( "SELECT * FROM contacts", null);
        mContactArrayAdapter.swapCursor(c);
    }


}
