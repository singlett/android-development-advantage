package com.rdevblog.contact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.sql.Blob;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Radther on 09/11/14.
 */

public class ContactsDatabaseHelper extends SQLiteOpenHelper {

    //database helpers instance as it is a singleton
    private static ContactsDatabaseHelper contactsDatabaseHelper;

    //make its self an instance.
    public static ContactsDatabaseHelper getInstance(Context context){

        if (contactsDatabaseHelper == null){
            contactsDatabaseHelper = new ContactsDatabaseHelper(context);
        }
        return contactsDatabaseHelper;
    }

    //talbe name for the database
    public static final String TABLE_CONTACTS = "contacts";

    //column names for the table in the database
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_SECOND_NAME = "second_name";
    public static final String COLUMN_CONTACT_NUMBER = "contact_number";
    public static final String COLUMN_ADDRESS1 = "address_line_1";
    public static final String COLUMN_ADDRESS2 = "address_line_2";
    public static final String COLUMN_POSTCODE = "address_postcode";
    public static final String COLUMN_IMAGE = "contact_image";

    //list of all the columns
    private static final String[] COLUMNS = {COLUMN_ID, COLUMN_FIRST_NAME, COLUMN_SECOND_NAME, COLUMN_CONTACT_NUMBER, COLUMN_ADDRESS1,COLUMN_ADDRESS2, COLUMN_POSTCODE};

    //database name
    private static final String DATABASE_NAME = "contacts.db";

    //database version. I went through quite a lot of testing here which is why it's so high.
    private static final int DATABASE_VERSION = 20100;

    //query to create the database and table
    private static final String DATABASE_CREATE = "create table " + TABLE_CONTACTS
            + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_FIRST_NAME + " text,"
            + COLUMN_SECOND_NAME + " text,"
            + COLUMN_CONTACT_NUMBER + " text,"
            + COLUMN_ADDRESS1 + " text,"
            + COLUMN_ADDRESS2 + " text,"
            + COLUMN_POSTCODE + " text,"
            + COLUMN_IMAGE+ " blob);";

    //private constructor
    private ContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //execute the table creation
        db.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //update the table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        this.onCreate(db);
    }


    //add command to make a new contact in the database
    public void addContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRST_NAME, contact.getmContactFirstName());
        values.put(COLUMN_SECOND_NAME, contact.getmContactSecondName());
        values.put(COLUMN_CONTACT_NUMBER, contact.getmContactNumber());
        values.put(COLUMN_ADDRESS1, contact.getmContactAddressLine1());
        values.put(COLUMN_ADDRESS2, contact.getmContactAddressLine2());
        values.put(COLUMN_POSTCODE, contact.getmContactPostcode());
//        values.put(COLUMN_IMAGE, contact.getmContactImageByte());

        db.insert(TABLE_CONTACTS, null, values);
//        db.close();

    }

    //Get a single contact from the database
    public Contact getContact(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS,
                COLUMNS,
                " "+COLUMN_ID+" = ?",
                new String[]{String.valueOf(id)},
                null, null, null, null);

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();}

        Contact contact = new Contact();
        contact.setmContactID(Integer.parseInt(cursor.getString(0)));
        contact.setmContactFirstName(cursor.getString(1));
        contact.setmContactSecondName(cursor.getString(2));
        contact.setmContactNumber(cursor.getString(3));
        contact.setmContactAddressLine1(cursor.getString(4));
        contact.setmContactAddressLine2(cursor.getString(5));
        contact.setmContactPostcode(cursor.getString(6));
//        Log.d("Column count",String.valueOf( cursor.getColumnCount()));
//        byte[] bytes = cursor.getBlob(7);
//        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//        contact.setmContactImage(bitmap);


//        db.close();
        return contact;


    }

    //get all the contacts from the database
    public List<Contact> getAllContacts(){
        List<Contact> contacts = new LinkedList<Contact>();

        String query = "SELECT * FROM "+TABLE_CONTACTS + " ORDER BY "+ COLUMN_SECOND_NAME + " COLLATE NOCASE";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        Contact contact = null;

        if (cursor.moveToFirst()){
            do {
                contact = new Contact();
                contact.setmContactID(Integer.parseInt(cursor.getString(0)));
                contact.setmContactFirstName(cursor.getString(1));
                contact.setmContactSecondName(cursor.getString(2));
                contact.setmContactNumber(cursor.getString(3));
                contact.setmContactAddressLine1(cursor.getString(4));
                contact.setmContactAddressLine2(cursor.getString(5));
                contact.setmContactPostcode(cursor.getString(6));

                contacts.add(contact);
            }while (cursor.moveToNext());
        }

//        db.close();

        return contacts;
    }

    //update a contact in the database
    public int updateContact(Contact contact){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_FIRST_NAME, contact.getmContactFirstName());
        values.put(COLUMN_SECOND_NAME, contact.getmContactSecondName());
        values.put(COLUMN_CONTACT_NUMBER, contact.getmContactNumber());
        values.put(COLUMN_ADDRESS1, contact.getmContactAddressLine1());
        values.put(COLUMN_ADDRESS2, contact.getmContactAddressLine2());
        values.put(COLUMN_POSTCODE, contact.getmContactPostcode());
//        values.put(COLUMN_IMAGE, contact.getmContactImageByte());

        int i = db.update(TABLE_CONTACTS, values, COLUMN_ID+" = ?", new String[] {String.valueOf(contact.getmContactID())});

//        db.close();

        return i;

    }

    //delete a contact in the database
    public void delete(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, COLUMN_ID+" = ?",
                new String[]{String.valueOf(contact.getmContactID())});
    }

    public String[] getColumns(){
        return COLUMNS;
    }

}
