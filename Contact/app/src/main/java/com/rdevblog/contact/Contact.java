package com.rdevblog.contact;

import android.graphics.Bitmap;
import android.media.Image;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

//custom contact object for storing the contacts info.
public class Contact {

    private String mContactFirstName;
    private String mContactSecondName;
    private Bitmap mContactImage;
    private String mContactNumber;
    private String mContactAddressLine1;
    private String mContactAddressLine2;
    private String mContactPostcode;

    private int mContactID;

    public Contact() {
        mContactFirstName = "";
        mContactSecondName = "";
        mContactNumber = "";
        mContactAddressLine1 = "";
        mContactAddressLine2 = "";
        mContactPostcode = "";
        mContactImage = null;

    }

    public String getmContactFirstName() {
        return mContactFirstName;
    }

    public void setmContactFirstName(String mContactFirstName) {
        this.mContactFirstName = mContactFirstName;
    }

    public String getmContactSecondName() {
        return mContactSecondName;
    }

    public void setmContactSecondName(String mContactSecondName) {
        this.mContactSecondName = mContactSecondName;
    }

    public String getmContactAddressLine1() {
        return mContactAddressLine1;
    }

    public void setmContactAddressLine1(String mContactAddressLine1) {
        this.mContactAddressLine1 = mContactAddressLine1;
    }

    public String getmContactAddressLine2() {
        return mContactAddressLine2;
    }

    public void setmContactAddressLine2(String mContactAddressLine2) {
        this.mContactAddressLine2 = mContactAddressLine2;
    }

    public String getmContactPostcode() {
        return mContactPostcode;
    }

    public void setmContactPostcode(String mContactPostcode) {
        this.mContactPostcode = mContactPostcode;
    }

    public Bitmap getmContactImage() {
        return mContactImage;
    }

    public byte[] getmContactImageByte(){
        if (mContactImage == null) {
            return null;
        }
        else {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            mContactImage.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] buffer = outputStream.toByteArray();
            return buffer;
        }
    }


    public void setmContactImage(Bitmap mContactImage) {
        this.mContactImage = mContactImage;
    }


    public String getmContactNumber() {
        return mContactNumber;
    }

    public void setmContactNumber(String mContactNumber) {
        this.mContactNumber = mContactNumber;
    }

    public int getmContactID() {
        return mContactID;
    }

    public void setmContactID(int mContactID) {
        this.mContactID = mContactID;
    }



    @Override
    public String toString() {
        if (mContactSecondName == ""){
            return mContactFirstName;
        }
        else {
            return mContactFirstName + " " + mContactSecondName;
        }
    }
}
