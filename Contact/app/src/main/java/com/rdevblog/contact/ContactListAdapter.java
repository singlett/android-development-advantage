package com.rdevblog.contact;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
* Created by Radther on 09/11/14.
*/

//custom cursor adapter for showing items in a list
public class ContactListAdapter extends CursorAdapter{

    private LayoutInflater mInflator;


    public ContactListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

        mInflator = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        //get the textviews for the content
        TextView contactNameTextView = (TextView)view.findViewById(R.id.list_item_contact_name);
        TextView contactNumberTextView = (TextView)view.findViewById(R.id.list_item_contact_number);

        String[] COLUMNS = ContactsDatabaseHelper.getInstance(context).getColumns();

        //set the text for the views
        contactNameTextView.setText(ContactsDatabaseHelper
                .getInstance(context)
                .getContact(Long.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(COLUMNS[0]))))
                .toString());

        contactNumberTextView.setText(ContactsDatabaseHelper
                .getInstance(context)
                .getContact(Long.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(COLUMNS[0]))))
                .getmContactNumber());

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflator.inflate(R.layout.contact_item_layout, null);
    }

    @Override
    public Object getItem(int position) {
        //get the position of an item
        return super.getItem(position);
    }


}
