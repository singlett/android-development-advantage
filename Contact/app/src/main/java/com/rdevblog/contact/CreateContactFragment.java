package com.rdevblog.contact;

import android.app.Activity;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A placeholder fragment containing a simple view.
 */
public class CreateContactFragment extends Fragment {

    private Contact mContact;

    private EditText editContactFirstName;
    private EditText editContactSecondName;
    private EditText editContactNumber;
    private EditText editContactAddressLine1Name;
    private EditText editContactAddressLine2Name;
    private EditText editContactPostcode;

    private ImageView editContactImage;

    private Button saveContactButton;

    private boolean update = false;

    boolean mIsDualPane=false;

    OnSaveClicked mCallback;

    public CreateContactFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();

        //check the intent extras and assign the contact if there is one.
        if (extras != null){
            Long contactId = extras.getLong("contact_id");
            Log.d("SOmething", String.valueOf(contactId));
            if (contactId!=0) {
                mContact = ContactsDatabaseHelper.getInstance(getActivity()).getContact(extras.getLong("contact_id"));
                update = true;
            }
            else {
                mContact = new Contact();
            }
             mIsDualPane = extras.getBoolean("is_dual_pane", false);
        }
        else {
            mContact = new Contact();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_contact, container, false);

        // Assign to views
        editContactFirstName = (EditText)rootView.findViewById(R.id.edit_first_name);
        editContactSecondName = (EditText)rootView.findViewById(R.id.edit_second_name);
        editContactNumber = (EditText)rootView.findViewById(R.id.edit_number);
        editContactAddressLine1Name = (EditText)rootView.findViewById(R.id.edit_first_line_address);
        editContactAddressLine2Name = (EditText)rootView.findViewById(R.id.edit_second_line_address);
        editContactPostcode = (EditText)rootView.findViewById(R.id.edit_postcode);
        editContactImage = (ImageView)rootView.findViewById(R.id.edit_contact_image);
        saveContactButton = (Button)rootView.findViewById(R.id.save_contact_button);

        //set the text
        editContactFirstName.setText(mContact.getmContactFirstName());
        editContactSecondName.setText(mContact.getmContactSecondName());
        editContactNumber.setText(mContact.getmContactNumber());
        editContactAddressLine1Name.setText(mContact.getmContactAddressLine1());
        editContactAddressLine2Name.setText(mContact.getmContactAddressLine2());
        editContactPostcode.setText(mContact.getmContactPostcode());
        editContactImage.setImageBitmap(mContact.getmContactImage());

        // Set text changed listeners and set the text to the contacts
        editContactFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            //Todo on text changed listner
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactFirstName(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editContactSecondName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactSecondName(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editContactNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactNumber(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editContactAddressLine1Name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactAddressLine1(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editContactAddressLine2Name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactAddressLine2(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editContactPostcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mContact.setmContactPostcode(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Save Button
        saveContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (update){
                    ContactsDatabaseHelper.getInstance(getActivity()).updateContact(mContact);
                }
                else {
                    ContactsDatabaseHelper.getInstance(getActivity()).addContact(mContact);
                    update = true;
                }
                mCallback.onSaveClicked();
            }
        });

        // Image view button
        editContactImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        return rootView;
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int PIC_CROP = 2;
    private Uri picUri;


    //handle the take a picture intent
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK){

            //handle the result of the camera intent, the picture.
            Bundle cameraBundle = data.getExtras();
            Bitmap cameraImage = (Bitmap) cameraBundle.get("data");
            editContactImage.setImageBitmap(cameraImage);
            mContact.setmContactImage(cameraImage);
//            performCrop();

        }

    }


    public interface OnSaveClicked {
        public void onSaveClicked();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnSaveClicked) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}

