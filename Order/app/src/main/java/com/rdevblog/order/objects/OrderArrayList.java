package com.rdevblog.order.objects;

import java.util.ArrayList;

/**
 * Created by TomSingleton on 08/12/14.
 */

/** this is an array list that contains all of the orders. It is a singleton so that the data is
 * can be easily accessed anywhere in the app.*/
public class OrderArrayList {
    private static OrderArrayList ourInstance = new OrderArrayList();

    public static OrderArrayList getInstance() {
        return ourInstance;
    }

    private ArrayList<Order> orderArrayList = new ArrayList<>();

    private OrderArrayList() {
    }

    public void AddOrder(Order order){
        orderArrayList.add(order);
    }

    public Order GetOrder(int position){
        return orderArrayList.get(position);
    }

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }
}
