package com.rdevblog.order;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.rdevblog.order.objects.Item;
import com.rdevblog.order.objects.Order;
import com.rdevblog.order.objects.OrderArrayList;

import java.util.UUID;


/**This is the activity for making a new order.*/
public class NewOrderActivity extends ActionBarActivity {

    Toolbar toolbar;

    LinearLayout cardListView;

    /**This method creates the view*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        int position = getIntent().getExtras().getInt("OrderFromArray"); // get extras from the intent if there are any

        cardListView = (LinearLayout)findViewById(R.id.card_list); //get the linear layout for the items in the list

        if (position !=-1){ //see if it's a new order or opening a previous one
            Order order = OrderArrayList.getInstance().GetOrder(position);

            /*Add all the items to the order if there are any too add*/
            for (int i = 0; i < order.getItems().size(); i++) {
                Item item = order.getItems().get(i);
                //create an inflater to make a new view
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                //make the new view
                View newCard = inflater.inflate(R.layout.order_card_view, null);
                //sort the spinner out in the new view
                Spinner foodSpinner = (Spinner)newCard.findViewById(R.id.food_title);
                ArrayAdapter<CharSequence> itemAdapter   = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.items, R.layout.custom_spinner_list_item);
                itemAdapter.setDropDownViewResource(R.layout.custom_spinner_list_item);
                foodSpinner.setAdapter(itemAdapter);

                //set the edittext text.
                EditText editText = (EditText)newCard.findViewById(R.id.food_price);
                editText.setText(String.valueOf(item.getFoodPrice()));

                //add view to the linear layout
                cardListView.addView(newCard);

            }

        }

        //Find the toolbar by id and set it as the action bar for the activity
        toolbar = (Toolbar)findViewById(R.id.new_order_toolbar);
        setSupportActionBar(toolbar);

        //the button for the fab
        ImageButton plusButton = (ImageButton)findViewById(R.id.new_view_button);

        //An onclick listner to see when the button is pressed.
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /** adds a new view to the linear layout*/
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View newCard = inflater.inflate(R.layout.order_card_view, null);
                Spinner foodSpinner = (Spinner)newCard.findViewById(R.id.food_title);
                ArrayAdapter<CharSequence> itemAdapter   = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.items, R.layout.custom_spinner_list_item);
                itemAdapter.setDropDownViewResource(R.layout.custom_spinner_list_item);
                foodSpinner.setAdapter(itemAdapter);

                cardListView.addView(newCard);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        /** when the confirm button is pressed the order is saved.*/
        if (id == R.id.action_settings) {
            LinearLayout view;
            CardView itemView;
            Spinner itemTitle;
            EditText itemPrice;
            Item orderItem;
            Order mOrder = new Order();

            //Loops through all the items in the order and saves them to the order.
            for (int i = 0; i < cardListView.getChildCount(); i++) {
                view = (LinearLayout)cardListView.getChildAt(i);
                for (int j = 0; j < view.getChildCount(); j++) {
                    itemView = (CardView)view.getChildAt(j);
                    itemTitle = (Spinner)itemView.findViewById(R.id.food_title);
                    itemPrice = (EditText)itemView.findViewById(R.id.food_price);
                    orderItem = new Item();
                    orderItem.setFoodName(itemTitle.getSelectedItem().toString());
                    try {
                        orderItem.setFoodPrice(Float.valueOf(itemPrice.getText().toString()));
                    }catch (NumberFormatException e){
                        orderItem.setFoodPrice(0);
                    }
                    mOrder.getItems().add(orderItem);
                }
            }
            //make and add the order to the array singleton
            mOrder.setOrderName("Order: "+String.valueOf(OrderArrayList.getInstance().getOrderArrayList().size()+1));
            OrderArrayList.getInstance().AddOrder(mOrder);

            //close the activity
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
