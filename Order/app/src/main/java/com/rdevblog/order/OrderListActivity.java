package com.rdevblog.order;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.rdevblog.order.objects.Order;
import com.rdevblog.order.objects.OrderArrayList;


public class OrderListActivity extends ActionBarActivity {

    private ListView mOrderListView;

    private ArrayAdapter<Order> mAdapter;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        //get and set the toolbar from the xml to the activity
        toolbar = (Toolbar)findViewById(R.id.order_list_view_toolbar);
        setSupportActionBar(toolbar);

        //get the list view
        mOrderListView = (ListView)findViewById(R.id.order_listview);

        //make a listview array adapter to handle items in the list and add it to the list view
        mAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.order_list_item, OrderArrayList.getInstance().getOrderArrayList());
        mOrderListView.setAdapter(mAdapter);

        //set the on item click listener to make a new activity with the relevent information, in this case the position of the item clicked.
        mOrderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), NewOrderActivity.class);
                intent.putExtra("OrderFromArray", position);
                startActivity(intent);
            }
        });

        //Fab button to start a new orer
        ImageButton imageButton = (ImageButton)findViewById(R.id.new_order_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewOrderActivity.class);
                intent.putExtra("OrderFromArray", -1);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_order_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mAdapter.notifyDataSetChanged();
    }
}
