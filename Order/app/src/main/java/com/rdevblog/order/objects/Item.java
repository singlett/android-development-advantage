package com.rdevblog.order.objects;

/**
 * Created by TomSingleton on 08/12/14.
 */

/** Custom object for the items in an order*/
public class Item {

    private String foodName;
    private float foodPrice;

    public Item() {

    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public float getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(float foodPrice) {
        this.foodPrice = foodPrice;
    }
}
