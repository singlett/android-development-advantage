package com.rdevblog.order.objects;

import java.util.ArrayList;

/**
 * Created by TomSingleton on 08/12/14.
 */

/** Cutom object for an order tha is made up of the item object*/
public class Order {

    private ArrayList<Item> items = new ArrayList<>();
    private String orderName;

    public Order() {

    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @Override
    public String toString() {
        return this.orderName;
    }
}
